from django.shortcuts import render, redirect
from .models import Project
from django.contrib.auth.decorators import login_required
from .forms import CreateForm


@login_required
def list_projects(request):  # create view for all list
    lists = Project.objects.filter(owner=request.user)
    context = {
        "lists": lists,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):  # create view for all list
    project = Project.objects.get(id=id)
    context = {
        "project": project,
    }
    return render(request, "projects/detail.html", context)


def create_project(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
