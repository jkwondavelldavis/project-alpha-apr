from django.urls import path
from .views import user_login, logout_user, signup


urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", logout_user, name="logout"),
    path("signup/", signup, name="signup"),
]
