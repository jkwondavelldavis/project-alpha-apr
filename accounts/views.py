from django.shortcuts import render, redirect
from accounts.forms import AccountsForm, SignUpForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User


def user_login(request):
    if request.method == "POST":
        form = AccountsForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

        user = authenticate(
            request,
            username=username,
            password=password,
        )
        if user is not None:
            login(request, user)
            return redirect("home")

    else:
        form = AccountsForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def logout_user(request):
    logout(request)
    return redirect("login")


def signup(request):
    form = SignUpForm(request.POST)
    if form.is_valid():
        username = form.cleaned_data["username"]
        password = form.cleaned_data["password"]
        confirm = form.cleaned_data["password_confirmation"]

        if confirm == password:
            user = User.objects.create_user(
                username,
                password=password,
            )
            login(request, user)
            return redirect("list_projects")
        else:
            form.add_error("password", "Passwords do not match")
    else:
        form = SignUpForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)
